﻿using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Dapper;
using ShellEnergyCodingTest.Core.Domain;
using ShellEnergyCodingTest.Core.Domain.Repositories;
using ShellEnergyCodingTest.Core.Extensions;

namespace ShellEnergyCodingTest.DataAccess.Repositories
{
    public abstract class DapperRepositoryBase<T> : IRepository<T>
    {
        protected readonly IDbConnection Connection;

        protected DapperRepositoryBase(IDbConnection connection)
        {
            Connection = connection;
        }

        public abstract string ImportStoredProcedureName { get; }
        public abstract string TableTypeName { get; }

        public async Task Import(IEnumerable<T> data)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@records", data.ToDataTable().AsTableValuedParameter($"dbo.{TableTypeName}"));
            await Connection.QueryAsync($"{ImportStoredProcedureName}", parameters, commandType: CommandType.StoredProcedure);
        }
    }
}