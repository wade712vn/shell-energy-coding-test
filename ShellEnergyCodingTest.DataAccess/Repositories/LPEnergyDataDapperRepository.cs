﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using ShellEnergyCodingTest.Core.Domain;

namespace ShellEnergyCodingTest.DataAccess.Repositories
{
    public class LPEnergyDataDapperRepository : DapperRepositoryBase<LPEnergyReading>
    {
        public LPEnergyDataDapperRepository(IDbConnection connection) : base(connection)
        {

        }

        public override string ImportStoredProcedureName => "SP_ImportLPEnergyData";
        public override string TableTypeName => "LPEnergyDataList";
    }
}
