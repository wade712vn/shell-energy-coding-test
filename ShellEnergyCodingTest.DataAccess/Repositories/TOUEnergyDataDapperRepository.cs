﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using ShellEnergyCodingTest.Core.Domain;

namespace ShellEnergyCodingTest.DataAccess.Repositories
{
    public class TOUEnergyDataDapperRepository : DapperRepositoryBase<TOUEnergyReading>
    {
        public TOUEnergyDataDapperRepository(IDbConnection connection) : base(connection)
        {

        }

        public override string ImportStoredProcedureName => "SP_ImportTOUEnergyData";
        public override string TableTypeName => "TOUEnergyDataList";
    }
}
