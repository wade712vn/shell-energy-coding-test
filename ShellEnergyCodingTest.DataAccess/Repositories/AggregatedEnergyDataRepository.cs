﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using ShellEnergyCodingTest.Core.Domain;
using ShellEnergyCodingTest.Core.Domain.Repositories;

namespace ShellEnergyCodingTest.DataAccess.Repositories
{
    public class AggregatedEnergyDataRepository : DapperRepositoryBase<AggregatedEnergyData>, IAggregatedEnergyDataRepository
    {
        public AggregatedEnergyDataRepository(IDbConnection connection) : base(connection)
        {
        }

        public override string ImportStoredProcedureName => "SP_ImportAggregatedEnergyData";
        public override string TableTypeName => "AggregatedEnergyDataList";
        public async Task<IEnumerable<AggregatedEnergyData>> GetEnergyData(long meter, string dataType, DateTime dateTime)
        {
            var sql = "SELECT TOP (1000) [Meter] ,[Date] ,[DataType] ,[Max] ,[Min] ,[Median] FROM [AggregatedEnergyData] WHERE Meter = @ AND DataType = @DataType AND Date = @Data ORDER BY Date ASC";
            return await Connection.QueryAsync<AggregatedEnergyData>(sql, new
            {
                Meter = meter,
                DataType = dataType,
                Date = dateTime.Date
            });
        }
    }
}
