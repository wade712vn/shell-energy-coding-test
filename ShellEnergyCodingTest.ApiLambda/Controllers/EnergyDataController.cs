using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ShellEnergyCodingTest.ApiLambda.Services;
using ShellEnergyCodingTest.ApiLambda.Services.Dtos;

namespace ShellEnergyCodingTest.ApiLambda.Controllers
{
    [Route("api/[controller]")]
    public class EnergyDataController : ControllerBase
    {
        private readonly IEnergyDataService _energyDataService;

        public EnergyDataController(IEnergyDataService energyDataService)
        {
            _energyDataService = energyDataService;
        }

        [HttpGet]
        public async Task<IEnumerable<AggregatedEnergyDataDto>> Get([FromQuery] long meter, [FromQuery] string dataType, [FromQuery] DateTime dateTime)
        {
            return await _energyDataService.GetEnergyData(meter, dataType, dateTime);
        }
    }
}
