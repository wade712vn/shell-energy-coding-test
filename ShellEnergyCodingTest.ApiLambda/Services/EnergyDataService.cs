﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ShellEnergyCodingTest.ApiLambda.Services.Dtos;
using ShellEnergyCodingTest.ApiLambda.Services.Mapping;
using ShellEnergyCodingTest.Core.Domain.Repositories;

namespace ShellEnergyCodingTest.ApiLambda.Services
{
    public class EnergyDataService : IEnergyDataService
    {
        private readonly IAggregatedEnergyDataRepository _repository;

        public EnergyDataService(IAggregatedEnergyDataRepository repository)
        {
            _repository = repository;
        }

        public async Task<IEnumerable<AggregatedEnergyDataDto>> GetEnergyData(long meter, string dataType, DateTime dateTime)
        {
            var energyData = await _repository.GetEnergyData(meter, dataType, dateTime);
            return energyData.Select(x => x.MapToDto());
        }
    }
}
