﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ShellEnergyCodingTest.ApiLambda.Services.Dtos;

namespace ShellEnergyCodingTest.ApiLambda.Services
{
    public interface IEnergyDataService
    {
        Task<IEnumerable<AggregatedEnergyDataDto>> GetEnergyData(long meter, string dataType, DateTime dateTime);
    }
}