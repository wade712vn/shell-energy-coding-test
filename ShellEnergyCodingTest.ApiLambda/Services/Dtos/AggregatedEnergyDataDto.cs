﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShellEnergyCodingTest.ApiLambda.Services.Dtos
{
    public class AggregatedEnergyDataDto
    {
        public long Meter { get; set; }
        public DateTime Date { get; set; }
        public string DataType { get; set; }
        public decimal Max { get; set; }
        public decimal Min { get; set; }
        public decimal Median { get; set; }
    }
}
