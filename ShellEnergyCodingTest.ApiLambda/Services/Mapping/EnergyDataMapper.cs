﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ShellEnergyCodingTest.ApiLambda.Services.Dtos;
using ShellEnergyCodingTest.Core.Domain;

namespace ShellEnergyCodingTest.ApiLambda.Services.Mapping
{
    public static class EnergyDataMapper
    {
        public static AggregatedEnergyDataDto MapToDto(this AggregatedEnergyData energyData)
        {
            return new AggregatedEnergyDataDto()
            {
                Meter = energyData.Meter,
                DataType = energyData.DataType,
                Date = energyData.Date,
                Min = energyData.Min,
                Max = energyData.Max,
                Median = energyData.Median,
            };
        }
    }
}
