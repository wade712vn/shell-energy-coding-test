﻿using System;
using System.Collections.Generic;
using System.Text;
using CsvHelper.Configuration;
using ShellEnergyCodingTest.Core.Domain;

namespace ShellEnergyCodingTest.IngestCsvFilesLambda.Csv
{
    public sealed class LPEnergyReadingMap : ClassMap<LPEnergyReading>
    {
        public LPEnergyReadingMap()
        {
            Map(m => m.MeterPointCode).Name("MeterPoint Code");
            Map(m => m.SerialNumber).Name("Serial Number");
            Map(m => m.PlantCode).Name("Plant Code");
            Map(m => m.DateTime).Name("Date/Time").TypeConverterOption.Format("dd/MM/yyyy H:mm:ss").Default(new DateTime(1900, 1, 1));
            Map(m => m.DataType).Name("Data Type");
            Map(m => m.DataValue).Name("Data Value");
            Map(m => m.Units).Name("Units");
            Map(m => m.Status).Name("Status");
        }
    }
}
