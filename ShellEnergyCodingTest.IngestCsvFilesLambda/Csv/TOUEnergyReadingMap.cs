﻿using System;
using System.Collections.Generic;
using System.Text;
using CsvHelper.Configuration;
using ShellEnergyCodingTest.Core.Domain;

namespace ShellEnergyCodingTest.IngestCsvFilesLambda.Csv
{
    public sealed class TOUEnergyReadingMap : ClassMap<TOUEnergyReading>
    {
        public TOUEnergyReadingMap()
        {
            Map(m => m.MeterCode).Name("MeterCode");
            Map(m => m.Serial).Name("Serial");
            Map(m => m.PlantCode).Name("PlantCode");
            Map(m => m.DateTime).Name("DateTime").TypeConverterOption.Format("dd/MM/yyyy H:mm").Default(new DateTime(1900, 1, 1));
            Map(m => m.DataType).Name("DataType");
            Map(m => m.Quality).Name("Quality");
            Map(m => m.Stream).Name("Stream");
            Map(m => m.Energy).Name("Energy");
            Map(m => m.Units).Name("Units");
        }
    }
}
