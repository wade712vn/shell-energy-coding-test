﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using CsvHelper;
using ShellEnergyCodingTest.Core;

namespace ShellEnergyCodingTest.IngestCsvFilesLambda.Csv
{
    public class CsvFileRecordReader : IRecordReader
    {
        public IEnumerable<T> ReadRecords<T>(Stream stream)
        {
            using var reader = new StreamReader(stream);
            using var csvReader = new CsvReader(reader, CultureInfo.InvariantCulture);
            csvReader.Context.RegisterClassMap<LPEnergyReadingMap>();
            csvReader.Context.RegisterClassMap<TOUEnergyReadingMap>();

            return csvReader.GetRecords<T>().ToList();
        }
    }
}
