using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Amazon.Lambda.Core;
using Amazon.Lambda.SQSEvents;
using Amazon.S3;
using Amazon.S3.Util;
using CsvHelper;
using Microsoft.Extensions.DependencyInjection;
using ShellEnergyCodingTest.Core;
using ShellEnergyCodingTest.Core.Domain;
using ShellEnergyCodingTest.Core.Domain.Repositories;
using ShellEnergyCodingTest.Core.Importers;
using ShellEnergyCodingTest.DataAccess.Repositories;
using ShellEnergyCodingTest.IngestCsvFilesLambda.Csv;


// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]

namespace ShellEnergyCodingTest.IngestCsvFilesLambda
{
    public class Function
    {
        private readonly IAmazonS3 _s3Client;

        public Function()
        {
            _s3Client = new AmazonS3Client();
        }

        public Function(IAmazonS3 s3Client)
        {
            _s3Client = s3Client;
        }

        /// <summary>
        /// This method is called for every Lambda invocation. This method takes in an SQS event object and can be used 
        /// to respond to SQS messages.
        /// </summary>
        /// <param name="evnt"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task FunctionHandler(SQSEvent evnt, ILambdaContext context)
        {
            var serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);
            var serviceProvider = serviceCollection.BuildServiceProvider();

            foreach (var message in evnt.Records)
            {
                var s3Event = S3EventNotification.ParseJson(message.Body);
                foreach (var s3Message in s3Event.Records)
                {
                    await ProcessMessageAsync(serviceProvider, s3Message, context);
                }
            }
        }

        private static void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IDbConnection>(serviceProvider => new SqlConnection(Environment.GetEnvironmentVariable("DefaultConnection")));
            services.AddTransient<IImporterFactory, ImporterFactory>();
            services.AddTransient<IRepository<LPEnergyReading>, LPEnergyDataDapperRepository>();
            services.AddTransient<IRepository<TOUEnergyReading>, TOUEnergyDataDapperRepository>();
            services.AddTransient<IRecordReader, CsvFileRecordReader>();
            services.AddTransient<LPEnergyDataImporter>();
            services.AddTransient<TOUEnergyDataImporter>();
            services.AddSingleton<IImporterFactory, ImporterFactory>();
        }

        private async Task ProcessMessageAsync(IServiceProvider serviceProvider, S3EventNotification.S3EventNotificationRecord message, ILambdaContext context)
        {
            try
            {
                var s3Entity = message.S3;
                
                context.Logger.LogLine($"Processing object {s3Entity.Object.Key}");
                
                using var s3Object = await _s3Client.GetObjectAsync(s3Entity.Bucket.Name, s3Entity.Object.Key);

                var regex = new Regex(@"energy-data/(?<type>\w+)/(?<name>\w+)");
                var match = regex.Match(s3Object.Key);

                if (match.Success)
                {
                    var importerFactory = serviceProvider.GetService<IImporterFactory>();
                    var recordType = match.Groups["type"].Value;
                    var importer = importerFactory.Create(recordType);
                    await importer.ImportFromStream(s3Object.ResponseStream);

                    context.Logger.LogLine($"Imported data from file {match.Groups["name"]}");
                }
                else
                {
                    context.Logger.LogLine($"File name doesn't match");
                }

            }
            catch (Exception e)
            {
                context.Logger.LogLine($"Error: {e.Message} - {e.StackTrace}");
            }

            context.Logger.LogLine($"Finished processing");

            await Task.CompletedTask;
        }
    }
}
