﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;

namespace ShellEnergyCodingTest.Core.Extensions
{
    public static class EnumerableExtensions
    {
        public static DataTable ToDataTable<T>(this IEnumerable<T> data)
        {
            var dataTable = new DataTable();

            var properties = typeof(T).GetProperties();

            foreach (var property in properties)
            {
                var propertyType = property.PropertyType;
                if (propertyType.IsGenericType && propertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                {
                    propertyType = Nullable.GetUnderlyingType(propertyType);
                }

                if (propertyType == null)
                    continue;

                dataTable.Columns.Add(property.Name, propertyType);
            }

            foreach (var item in data)
            {
                var row = dataTable.NewRow();
                foreach (var property in properties)
                {
                    var propertyValue = property.GetValue(item);
                    row[property.Name] = propertyValue ?? DBNull.Value;
                }

                dataTable.Rows.Add(row);
            }
            return dataTable;
        }
    }
}
