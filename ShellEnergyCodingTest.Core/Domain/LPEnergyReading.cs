﻿using System;

namespace ShellEnergyCodingTest.Core.Domain
{
    public class LPEnergyReading
    {
        public long MeterPointCode { get; set; }
        public long SerialNumber { get; set; }
        public string PlantCode { get; set; }
        public DateTime DateTime { get; set; }
        public string DataType { get; set; }
        public decimal DataValue { get; set; }
        public string Units { get; set; }
        public string Status { get; set; }
    }
}
