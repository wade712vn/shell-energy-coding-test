﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShellEnergyCodingTest.Core.Domain
{
    public class AggregatedEnergyData
    {
        public long Meter { get; set; }
        public DateTime Date { get; set; }
        public string DataType { get; set; }
        public decimal Max { get; set; }
        public decimal Min { get; set; }
        public decimal Median { get; set; }
    }
}
