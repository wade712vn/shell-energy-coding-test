﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ShellEnergyCodingTest.Core.Domain.Repositories
{
    public interface IAggregatedEnergyDataRepository
    {
        Task<IEnumerable<AggregatedEnergyData>> GetEnergyData(long meter, string dataType, DateTime dateTime);
    }
}
