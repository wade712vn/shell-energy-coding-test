﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ShellEnergyCodingTest.Core.Domain.Repositories
{
    public interface IRepository<in T>
    {
        Task Import(IEnumerable<T> data);
    }
}