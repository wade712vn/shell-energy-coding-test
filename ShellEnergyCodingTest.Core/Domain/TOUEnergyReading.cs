﻿using System;

namespace ShellEnergyCodingTest.Core.Domain
{
    public class TOUEnergyReading
    {
        public long MeterCode { get; set; }
        public long Serial { get; set; }
        public string PlantCode { get; set; }
        public DateTime DateTime { get; set; }
        public string Quality { get; set; }
        public string Stream { get; set; }
        public string DataType { get; set; }
        public decimal Energy { get; set; }
        public string Units { get; set; }
    }
}
