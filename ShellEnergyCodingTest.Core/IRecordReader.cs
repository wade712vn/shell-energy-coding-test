﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ShellEnergyCodingTest.Core
{
    public interface IRecordReader
    {
        IEnumerable<T> ReadRecords<T>(Stream stream);
    }
}
