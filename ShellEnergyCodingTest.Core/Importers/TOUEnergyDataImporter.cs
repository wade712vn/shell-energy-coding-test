﻿using System.Globalization;
using ShellEnergyCodingTest.Core.Domain;
using ShellEnergyCodingTest.Core.Domain.Repositories;

namespace ShellEnergyCodingTest.Core.Importers
{
    public class TOUEnergyDataImporter : EnergyDataImporterBase<TOUEnergyReading>
    {
        public TOUEnergyDataImporter(IRecordReader recordReader, IRepository<TOUEnergyReading> repository) : base(recordReader, repository)
        {
        }
    }
}
