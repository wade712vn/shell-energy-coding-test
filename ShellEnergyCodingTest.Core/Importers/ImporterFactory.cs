﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShellEnergyCodingTest.Core.Importers
{
    public class ImporterFactory : IImporterFactory
    {
        private readonly IServiceProvider _serviceProvider;

        public ImporterFactory(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public IEnergyDataImporter Create(string recordType)
        {
            switch (recordType)
            {
                case "LP":
                    return (IEnergyDataImporter) _serviceProvider.GetService(typeof(LPEnergyDataImporter));
                case "TOU":
                    return (IEnergyDataImporter)_serviceProvider.GetService(typeof(TOUEnergyDataImporter));
            }

            throw new ArgumentException("Invalid record type");
        }
    }
}
