﻿using System.IO;
using System.Threading.Tasks;
using ShellEnergyCodingTest.Core.Domain;
using ShellEnergyCodingTest.Core.Domain.Repositories;

namespace ShellEnergyCodingTest.Core.Importers
{
    public abstract class EnergyDataImporterBase<T> : IEnergyDataImporter
    {
        private readonly IRecordReader _recordReader;
        private readonly IRepository<T> _repository;

        protected EnergyDataImporterBase(IRecordReader recordReader, IRepository<T> repository)
        {
            _recordReader = recordReader;
            _repository = repository;
        }

        public async Task ImportFromStream(Stream stream)
        {
            var records = _recordReader.ReadRecords<T>(stream);
            await _repository.Import(records);
        }
    }
}