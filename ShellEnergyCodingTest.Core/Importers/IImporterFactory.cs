﻿namespace ShellEnergyCodingTest.Core.Importers
{
    public interface IImporterFactory
    {
        IEnergyDataImporter Create(string recordType);
    }
}