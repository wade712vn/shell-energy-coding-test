﻿using System.Globalization;
using ShellEnergyCodingTest.Core.Domain;
using ShellEnergyCodingTest.Core.Domain.Repositories;

namespace ShellEnergyCodingTest.Core.Importers
{
    public class LPEnergyDataImporter : EnergyDataImporterBase<LPEnergyReading>
    {
        public LPEnergyDataImporter(IRecordReader recordReader, IRepository<LPEnergyReading> repository) : base(recordReader, repository)
        {
        }
    }
}
