﻿using System.IO;
using System.Threading.Tasks;

namespace ShellEnergyCodingTest.Core.Importers
{
    public interface IEnergyDataImporter
    {
        Task ImportFromStream(Stream stream);
    }
}